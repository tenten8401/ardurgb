#include "zonetab.h"
#include "ui_zonetab.h"
#include <QDebug>

ZoneTab::ZoneTab(int zoneTmp, int hueTmp, int saturationTmp, int valueTmp, int modeTmp, int delayTmp, QSerialPort& serialTmp, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ZoneTab)
{
    ui->setupUi(this);

    // Setup variables
    color = QColor(0, 0, 0);
    zone = zoneTmp;
    serial = &serialTmp;

    // Initial value population
    QStringList list;
    list.append("Solid");
    list.append("Breathing");
    list.append("Rainbow");
    ui->comboBox->clear();
    ui->comboBox->addItems(list);
    ui->comboBox->setCurrentIndex(modeTmp);

    qInfo() << QString("ZoneTab: %1, %2, %3, %4, %5, %6").arg(zoneTmp).arg(hueTmp).arg(saturationTmp).arg(valueTmp).arg(modeTmp).arg(delayTmp);

    int properHue = (static_cast<float>(360) / static_cast<int>(static_cast<float>(255))) * static_cast<float>(hueTmp);
    qInfo() << QString("Proper Hue: %1").arg(properHue);
    color.setHsv(properHue, saturationTmp, valueTmp);
    updateColorSliders();

    ui->delaySpinBox->setValue(delayTmp);
}

ZoneTab::~ZoneTab()
{
    delete ui;
}

void ZoneTab::on_updateButton_clicked()
{
    if(!serial->isOpen()) {
        QMessageBox msg;
        msg.setWindowTitle("Error");
        msg.setText("Serial port not connected!");
        msg.exec();
    } else {
        // Convert to 0-255 Hue for FastLED
        int hue = static_cast<int>(static_cast<float>(255) / static_cast<float>(360) * static_cast<float>(color.hslHue()));
        QString updatePacket = QString("update|%1|%2|%3|%4|%5|%6\n").arg(zone).arg(hue).arg(color.hslSaturation()).arg(color.value()).arg(ui->comboBox->currentIndex()).arg(ui->delaySpinBox->value());
        serial->write(updatePacket.toUtf8().constData());
    }
}

void ZoneTab::updateColorPreview() {
    color.setHsv(ui->hueSlider->value(), ui->saturationslider->value(), ui->valueSlider->value());
    QString style_sheet = QString("background-color: #%1;").arg(color.rgba(), 0, 16);
    ui->colorValueEdit->setText(QString("%1").arg(color.name()));
    ui->colorPreview->setStyleSheet(style_sheet);
}


void ZoneTab::updateColorSliders() {
    ui->hueSlider->setValue(color.hue());
    ui->saturationslider->setValue(color.saturation());
    ui->valueSlider->setValue(color.value());
    updateColorPreview();
}

void ZoneTab::on_colorValueEdit_editingFinished()
{
    QString hex = ui->colorValueEdit->text();
    if (hex[0] == '#')
        hex = hex.remove(0, 1);

    unsigned int r = hex.mid(0, 2).toUInt(nullptr, 16);
    unsigned int g = hex.mid(2, 2).toUInt(nullptr, 16);
    unsigned int b = hex.mid(4, 2).toUInt(nullptr, 16);
    qInfo() << QString("%1, %2, %3").arg(r).arg(g).arg(b);
    color.setRgb(r, g, b);
    updateColorSliders();
}


void ZoneTab::on_hueSlider_valueChanged()
{
    updateColorPreview();
}

void ZoneTab::on_saturationslider_valueChanged()
{
    updateColorPreview();
}

void ZoneTab::on_valueSlider_valueChanged()
{
    updateColorPreview();
}
