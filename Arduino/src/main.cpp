#include <Arduino.h>
#include <Configuration.h>
#include <EEPROM.h>
#include <FastLED.h>
#include <RGBDevice.h>
#include <ResponseHandler.h>

// RGB mode data
int breathingCounter = 0;
int breathDirection = 1;

String serialIn;
ResponseHandler validator;
RGBZone zones[NUM_ZONES];
RGBDevice devices[NUM_DEVICES];
int rainbowHue[NUM_DEVICES];
CRGB deviceRgb[NUM_DEVICES][150];
bool serialInterrupt[NUM_DEVICES];

bool delayWithInterrupt(int device, float multiplier) {
  if (serialInterrupt[device]) {
    return true;
  }
  vTaskDelay(zones[devices[device].zone].delay * multiplier);
  return false;
}

void solidEffect(int device) {
  RGBZone zone = zones[devices[device].zone];
  if (!devices[device].addressible) {
    // Analog
    CRGB color = CHSV(zone.hue, zone.saturation, zone.value);
    ledcWrite((device * 3) + 0, color.red);
    ledcWrite((device * 3) + 1, color.blue);
    ledcWrite((device * 3) + 2, color.blue);
  } else {
    // Digital
    for (int i2 = 0; i2 < devices[device].leds; i2++) {
      deviceRgb[device][i2] = CHSV(zone.hue, zone.saturation, zone.value);
    }
  }
  delayWithInterrupt(device, 1.0);
}

void breathingEffect(int device) {
  RGBZone zone = zones[devices[device].zone];
  if (!devices[device].addressible) {
    // Analog
    CRGB color = CHSV(zone.hue, zone.saturation, zone.value);
    for (int i = 0; i < 255; i++) {
      ledcWrite((device * 3) + 0, color.red * i);
      ledcWrite((device * 3) + 1, color.blue * i);
      ledcWrite((device * 3) + 2, color.blue * i);
      if (delayWithInterrupt(device, 1.0))
        break;
    }
    for (int i = 255; i > 0; i--) {
      ledcWrite((device * 3) + 0, color.red * i);
      ledcWrite((device * 3) + 1, color.blue * i);
      ledcWrite((device * 3) + 2, color.blue * i);
      if (delayWithInterrupt(device, 1.0))
        break;
    }
  } else {
    // Digital
    for (int i = 0; i < 255; i++) {
      for (int i2 = 0; i2 < devices[device].leds; i2++) {
        deviceRgb[device][i2] = CHSV(zone.hue, zone.saturation,
                                     zone.value * ((float)i / (float)255));
      }
      if (delayWithInterrupt(device, 1.0))
        break;
    }
    for (int i = 255; i > 0; i--) {
      for (int i2 = 0; i2 < devices[device].leds; i2++) {
        deviceRgb[device][i2] = CHSV(zone.hue, zone.saturation,
                                     zone.value * ((float)i / (float)255));
      }
      if (delayWithInterrupt(device, 1.0))
        break;
    }
  }
}

void rainbowEffect(int device) {
  RGBZone zone = zones[devices[device].zone];
  if (devices[device].addressible) {
    // Digital
    for (int i = 0; i < devices[device].leds; i++) {
      deviceRgb[device][i] =
          CHSV(rainbowHue[device] + (i * (255 / devices[device].leds)),
               zone.saturation, zone.value);
    }
    rainbowHue[device]++;
  }
  delayWithInterrupt(device, 1.0);
}

/*void slidingEffect(int zone) {
  if (device > ANALOG_DEVICES) {
    for (int i = 0; i < devices[device].leds; i++) {
      deviceRgb[device][i] =
          CHSV(devices[device].hue, devices[device].saturation,
               devices[device].value);
      if (delayWithInterrupt(device, 1.0))
        break;
      deviceRgb[device][i] = CRGB::Black;
    }
  }
}*/

void deviceController(void *pvParameters) {
  int device = (int)pvParameters;
  while (true) {
    int deviceMode = zones[devices[device].zone].mode;
    switch (deviceMode) {
    case 0:
      solidEffect(device);
      break;
    case 1:
      breathingEffect(device);
      break;
    case 2:
      rainbowEffect(device);
      break;
    /*case 3:
      slidingEffect(device);
      break;*/
    default:
      break;
    }
    serialInterrupt[device] = false;
  }
}

void showAndSerialTask(void *pvParameters) {
  while (true) {
    if (Serial.available() > 0) {
      serialIn = Serial.readStringUntil('\n');
      if (validator.getResponse(serialIn, zones, devices)) {
        int zone = validator.getValue(serialIn, '|', 1).toInt();
        for (int i = 0; i < NUM_DEVICES; i++) {
          if (devices[i].zone == zone) {
            serialInterrupt[i] = true;
          }
        }
        EEPROM.put(0, zones);
        EEPROM.commit();
      }
    } else {
      FastLED.show();
    }
    delay(25); // 40hz
  }
}

void setup() {
  Serial.begin(9600);
  Serial.setDebugOutput(false);

  if (!EEPROM.begin(EEPROM_SIZE)) {
    Serial.println("failed to initialise EEPROM");
    delay(1000000);
  }

  randomSeed(analogRead(4));

  // Check if EEPROM has been initialized
  if (EEPROM.read(EEPROM_SIZE - 1) == EEPROM_FLAG) {
    // Read zones from EEPROM
    EEPROM.get(0, zones);
    EEPROM.get(EEPROM_SIZE / 2, devices);
  } else {
    // Wipe EEPROM clean
    for (unsigned int i = 0; i < EEPROM_SIZE; i++) {
      EEPROM.write(i, 0);
    }
    // Initialize array
    for (int i = 0; i < NUM_ZONES; i++) {
      zones[i].hue = random(0, 255);
      zones[i].saturation = 255;
      zones[i].value = 255;
      zones[i].mode = 0;
      zones[i].delay = 10;
    }
    for(int i = 0; i < NUM_DEVICES; i++) {
      devices[i].zone = 0;
      devices[i].name = "Device " + String(i);
    }

    // write array & flag
    EEPROM.put(0, zones);
    EEPROM.put(EEPROM_SIZE / 2, devices);
    EEPROM.put(EEPROM_SIZE - 1, EEPROM_FLAG);
    EEPROM.commit();
  }

  // TODO: Implement stuff to change this
  devices[0].addressible = false;
  devices[0].name = "Device 0";
  devices[1].addressible = false;
  devices[1].name = "Device 1";
  devices[2].addressible = true;
  devices[2].leds = 6;
  devices[2].name = "Device 2";
  devices[3].addressible = true;
  devices[3].leds = 9;
  devices[3].name = "Device 3";
  devices[4].addressible = true;
  devices[4].leds = 9;
  devices[4].name = "Device 4";

  // Device 1
  ledcSetup(0, 5000, 8);
  ledcAttachPin(D1_RED, 0);
  ledcSetup(1, 5000, 8);
  ledcAttachPin(D1_BLUE, 1);
  ledcSetup(2, 5000, 8);
  ledcAttachPin(D1_GREEN, 2);

  // Device 2
  ledcSetup(3, 5000, 8);
  ledcAttachPin(D2_RED, 3);
  ledcSetup(4, 5000, 8);
  ledcAttachPin(D2_BLUE, 4);
  ledcSetup(5, 5000, 8);
  ledcAttachPin(D2_GREEN, 5);

  // Device 3
  FastLED.addLeds<D3_TYPE, D3_DATA, D3_ORDER>(deviceRgb[2], D3_LEDS);
  devices[2].leds = D3_LEDS;

  // Device 4
  FastLED.addLeds<D4_TYPE, D4_DATA, D4_ORDER>(deviceRgb[3], D4_LEDS);
  devices[3].leds = D4_LEDS;

  // Device 5
  FastLED.addLeds<D5_TYPE, D5_DATA, D5_ORDER>(deviceRgb[4], D5_LEDS);
  devices[4].leds = D5_LEDS;

  // Set up tasks
  xTaskCreate(showAndSerialTask, "showAndSerial", 4096, NULL, 3, NULL);

  // Create device controllers
  for (int i = 0; i < NUM_DEVICES; i++) {
    xTaskCreate(deviceController, "device" + i, 4096, (void *)i, 1, NULL);
  }
}

void loop() {}