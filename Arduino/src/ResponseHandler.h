#pragma once
#include <Arduino.h>
#include <RGBDevice.h>
#include <RGBZone.h>

class ResponseHandler {
public:
  String getValue(String data, char separator, int index);
  bool getResponse(String response, RGBZone *zones, RGBDevice *devices);
};