// Device 1 - Analog RGB
#define D1_RED 22
#define D1_GREEN 19
#define D1_BLUE 23

// Device 2 - Analog RGB
#define D2_RED 5
#define D2_GREEN 15
#define D2_BLUE 2

// Device 3 - Addressible RGB
#define D3_DATA 25
#define D3_LEDS 6
#define D3_TYPE WS2812B
#define D3_ORDER GRB

// Device 4 - Addressible RGB
#define D4_DATA 26
#define D4_LEDS 9
#define D4_TYPE WS2812B
#define D4_ORDER GRB

// Device 5 - Addressible RGB
#define D5_DATA 27
#define D5_LEDS 9
#define D5_TYPE WS2812B
#define D5_ORDER GRB

// Don't touch unless you update the device add process
#define NUM_ZONES 5
#define NUM_DEVICES 5
#define ANALOG_DEVICES 2
#define DIGITAL_DEVICES 3
#define EEPROM_SIZE 1024
#define EEPROM_FLAG 'G'